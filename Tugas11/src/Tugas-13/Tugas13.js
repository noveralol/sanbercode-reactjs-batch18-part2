import React, {useState, useEffect} from "react"
import axios from "axios"

const Tugas13 = () => {  
  const [daftarBuah, setDaftarBuah] =  useState(null)
  const [input, setInput]  =  useState({name: "", price: "", weight: 0, id: null})

  useEffect( () => {
    if (daftarBuah === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        setDaftarBuah(res.data.map(el=>{ return {id: el.id, name: el.name, price: el.price, weight: el.weight }} ))
      })
    }
  }, [daftarBuah])
  
  const handleDelete = (event) => {
    let idDataBuah = parseInt(event.target.value)

    let newdaftarBuah = daftarBuah.filter(el => el.id !== idDataBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
    .then(res => {
      console.log(res)
    })
          
    setDaftarBuah([...newdaftarBuah])
    
  }
  
  const handleEdit = (event) =>{
    let idDataBuah = parseInt(event.target.value)
    let dataBuah = daftarBuah.find(x=> x.id === idDataBuah)
    setInput({name: dataBuah.name, price: dataBuah.price, weight: dataBuah.weight, id: idDataBuah})
  }

  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "name":
      {
        setInput({...input, name: event.target.value});
        break
      }
      case "price":
      {
        setInput({...input, price: event.target.value});
        break
      }
      case "weight":
      {
        setInput({...input, weight: event.target.value});
          break
      }
    default:
      {break;}
    }
  }

  const handleSubmit = (event) =>{
    event.preventDefault()

    let name = input.name
    let price = input.price.toString()
    

    if (input.id === null){        
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight: input.weight})
      .then(res => {
          setDaftarBuah([
            ...daftarBuah, 
            { id: res.data.id, 
              name, 
              price,
              weight: input.weight
            }])
      })
    }else{
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {name, price, weight: input.weight})
      .then(() => {
          let dataBuah = daftarBuah.find(el=> el.id === input.id)
          dataBuah.name = name
          dataBuah.price = price
          dataBuah.weight = input.weight
          setDaftarBuah([...daftarBuah])
      })
    }

    setInput({name: "", price: "", weight: 0, id: null})

  }

  return(
    <>
      <center><h1>Tabel Harga Buah</h1></center>
      <br/>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>

            {
              daftarBuah !== null && daftarBuah.map((item, index)=>{
                return(                    
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.weight/1000} Kg</td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>
      <center>
      <h1>Tambah Tabel Harga Buah</h1>
      <div>
          <form onSubmit={handleSubmit}>
            <label>
              Nama:
            </label>
            <input type="text" required name="name" value={input.name} onChange={handleChange}/>
            <br/>
            <br/>
            <label>
              Harga:
            </label>
            <input type="text" required name="price" value={input.price} onChange={handleChange}/>
            <br/>
            <br/>
            <label>
              Berat (dalam gram):
            </label>
            <input type="number" required name="weight" value={input.weight} onChange={handleChange}/>
              <button style={{ marginLeft:"20px"}}>submit</button>
          </form>
        </div>
        </center>
    </>
  )
}

export default Tugas13
