import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 120,
      date: new Date(),
      showTime: true,
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1 
    });
  }

  componentDidUpdate(){
    if (this.state.time === 0){
      this.state.showTime = false
    }
  }
  

  render(){
    return(
      <>
      {
        this.state.showTime && (
        <h1 style={{textAlign: "center"}}>
        sekarang jam : {this.state.date.toLocaleTimeString()} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; hitung mundur : {this.state.time}
        </h1>
        )
      }
      </>
    )
  }
}

export default Timer