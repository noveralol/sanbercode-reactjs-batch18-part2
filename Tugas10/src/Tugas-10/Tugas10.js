import React from 'react';
import '../App.css';

class Fruits extends React.Component {
  render() {
    return <div>{this.props.nama}</div>
  }
  }

class Price extends React.Component {
  render() {
    return <div>{this.props.harga}</div>
  }
}

class Weight extends React.Component {
  render() {
    return <div>{this.props.berat/1000} kg</div>
  }
}


let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
];

function Tugas10() {
  return (
    <div className="Tugas10">
      <center><h1>Tabel Harga Buah</h1></center>
      <br/>
      <div className="Table">
      <table>
      <tr>
      <th>Nama</th>
      <th>Harga</th> 
      <th>Berat</th>
      </tr>
        {dataHargaBuah.map(data=>{
          return(
            <tr>
              <td><Fruits nama={data.nama} /></td>
              <td><Price harga={data.harga} /></td>
              <td><Weight berat={data.berat} /></td>
            </tr>
          )
        })}
      </table>
      </div>
    </div>
  );
};

export default Tugas10;