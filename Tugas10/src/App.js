import React from 'react';
import './App.css';
import Tugas9 from './Tugas-9/Tugas9.js';
import Tugas10 from './Tugas-10/Tugas10.js';

function App() {
  return (
    <div>
      <Tugas10 />
      <br />
      <Tugas9 />
    </div>
  );
};

export default App;
